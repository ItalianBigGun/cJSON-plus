#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON-plus.h"
#if 0
int cJSON_Show(cJSON *obj, int n)
{
    cJSON *tmp;
    cJSON *p;
    int i, j;
    int arraySize;

    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
    while (1) {
        if (!tmp) {
            if (p) {
                switch (p->type) {
                    case cJSON_Number:
                        printf("value:%d\n", p->valueint);
                        break;
                    case cJSON_String:
                        printf("value:%s\n", p->valuestring);
                        break;
                }
            }
            break;
        }

        if (tmp->type == cJSON_Array) {
            printf("array:%s, size:%d\n", tmp->string, cJSON_GetArraySize(tmp));
            arraySize = cJSON_GetArraySize(tmp);
            for (i = 0; i < arraySize; i++) {
                p = cJSON_GetArrayItem(tmp, i);
                    if (p) {
                        if (p->type == cJSON_Object)
                            cJSON_Show(p, n);
                        else {
                            cJSON_Show(p, n + 1);
                        }
                    } else {
                        printf("what?\n");
                    }
                }
            tmp = tmp->next;
            //cJSON_Show(tmp, n);       
        } else if (tmp->string && strcmp(tmp->string, "null")){
            printf("obj:%s\t", tmp->string);
            switch (tmp->type) {
                case cJSON_Number:
                    printf("value:%d\n", tmp->valueint);
                    break;
                case cJSON_String:
                    printf("value:%s\n", tmp->valuestring);
                    break;
                default:
                    printf("value:\n");
                    break;
            }
            tmp = tmp->next;
            //cJSON_Show(tmp, n);
        } else {
            switch (tmp->type) {
                case cJSON_Number:

                    printf("number:%lf", tmp->valuedouble);
                    break;
                case cJSON_String:
                    printf("string:%s\n", tmp->valuestring);
                    break;
            
            }
            tmp = tmp->next;
            //n++;
        }

    }

    return 0;
}
#endif
int main(int argc, const char *argv[])
{
        cJSON *mJson;
#if 1
        mJson = cJSON_Parse("{\"refresh_interval\":60,"
                        "\"wife\":[\"li\",\"zhuo\",\"cong\"],"
                        "\"timeout\":12.5,"
                        "\"dynamic_loading\":["
                            "{"
                                "\"so_path\":\"plugins/User.so\", \"load\":false, \"version\":1,"
                                "\"cmd\":["
                                     "{\"cmd\":2001, \"class\":\"neb::CmdUserLogin\"},"
                                     "{\"cmd\":2003, \"class\":\"neb::CmdUserLogout\"}"
                                "],"
                                "\"module\":["
                                     "{\"path\":\"im/user/login\", \"class\":\"neb::ModuleLogin\"},"
                                     "{\"path\":\"im/user/logout\", \"class\":\"neb::ModuleLogout\"}"
                                "]"
                             "},"
                             "{"
                             "\"so_path\":\"plugins/ChatMsg.so\", \"load\":false, \"version\":1,"
                                 "\"cmd\":["
                                      "{\"cmd\":2001, \"class\":\"neb::CmdChat\"}"
                                 "],"
                             "\"module\":[]"
                             "}"
                        "]"
                    "}");
#endif
    cJSON_ShowByTree(mJson, 0);
    cJSON_SetAllItem(mJson);
    cJSON_Show(mJson);
	return 0;
}
