# cJSON-plus

#### 介绍
CJSON extension library.

#### 软件功能

- 向前添加键值对。
- 修改指定域的字符串。
- 遍历cJSON对象。
- 其他格式化输出对象。（支持列表和树形等）
- 遍历修改所有项功能。
- 修改指定域的值。


#### 安装教程

1. 生成库
    - 使用./script/build.sh或者make。
    - 生成库文件到当前的lib目录。分别有.o,.a和.so类型文件。
2. 安装库
    - 使用./script/install.sh或者make install。
    - 将lib中的文件移动到/usr/local/lib目录中。
3. 清除库
    - 使用./script/clean.sh或者make clean。
    - 清空lib中的文件。

#### 使用说明
1. 在这个版本，请勿使用动态库链接。动态库暂时无法使用。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
 
=======
>>>>>>> 27da822a68cd81ea9d5a8399da6a21b7c4b37ed2
