# !/bin/sh

gcc -c ./src/cJSON.c -o ./lib/cJSON.o -I ./include
g++ -c ./src/cJSON-plus.cpp -o ./lib/cJSON-plus.o -I ./include

ar -r ./lib/cJSON.a ./lib/cJSON.o >> /dev/null
ar -r ./lib/cJSON-plus.a ./lib/cJSON.o ./lib/cJSON-plus.o >> /dev/null

gcc -fPIC -shared -o ./lib/cJSON-plus.so ./src/cJSON.c ./src/cJSON-plus.cpp -I ./include  
