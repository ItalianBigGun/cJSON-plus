/*
  Copyright (c) 2009 Dave Gamble
 
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
 
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __CJSON_PLUS_H__
#define __CJSON_PLUS_H__
#include <vector>
using namespace std;
#ifdef __cplusplus
extern "C" {
#endif //   __cplusplus
#include "cJSON.h"

typedef struct _cJSON_list {
	char 	*titleName;
	cJSON 	*listItem;
} cJSON_list;

int cJSON_GetObjectList(cJSON *obj, cJSON_list **list, int *size);
int cJSON_SetKeyValue(cJSON *obj, const char *key, const char *value);
int cJSON_SetAllItem(cJSON *obj);
int cJSON_SetItemKey(cJSON *obj, const char *ok, const char *nk);
int cJSON_ShowByTree(cJSON *obj, int n);
int cJSON_Show(cJSON *obj);

/* Append item in front of the specified array/object. */
void cJSON_AddItemToArrayR(cJSON *array, cJSON *item);
void cJSON_AddItemToObjectR(cJSON *object, const char *string, cJSON *item);

/* Use to appead in front of object. */
#define cJSON_AddNullToObjectR(object,name)        cJSON_AddItemToObjectR(object, name, cJSON_CreateNull())
#define cJSON_AddTrueToObjectR(object,name)        cJSON_AddItemToObjectR(object, name, cJSON_CreateTrue())
#define cJSON_AddFalseToObjectR(object,name)        cJSON_AddItemToObjectR(object, name, cJSON_CreateFalse())
#define cJSON_AddBoolToObjectR(object,name,b)    cJSON_AddItemToObjectR(object, name, cJSON_CreateBool(b))
#define cJSON_AddNumberToObjectR(object,name,n)    cJSON_AddItemToObjectR(object, name, cJSON_CreateNumber(n))
#define cJSON_AddStringToObjectR(object,name,s)    cJSON_AddItemToObjectR(object, name, cJSON_CreateString(s))


#ifdef __cplusplus
}
#endif //   __cplusplus

#endif // __CJSON_PLUS_H__