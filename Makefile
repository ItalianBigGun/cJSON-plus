INCLUDE := ./include

./lib/libcJSON-plus.so: ./src/cJSON.c ./src/cJSON-plus.cpp
	g++ -fPIC -shared -o $@ $^ -I $(INCLUDE)

.PHONY : clean

clean:
	-rm ./lib/libcJSON-plus.so

.PHONY : install

install:
	-cp ./lib /usr/local -r

.PHONY : INSTALL

INSTALL:
	-cp ./lib /usr/local -r
