/*
  Copyright (c) 2009 Dave Gamble

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include "cJSON-plus.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>
using namespace std;
/* Copy from cjSON library file. */
#if defined(WIN32)&&defined(_DEBUG)&&defined(_CRTDBG_MAP_ALLOC)
#define cJSON_malloc malloc
#define cJSON_free free
#else
static void *(*cJSON_malloc)(size_t sz) = malloc;
static void (*cJSON_free)(void *ptr) = free;
#endif

static char* cJSON_strdup(const char* str)
{
    size_t len;
    char* copy;

    len = strlen(str) + 1;
    if (!(copy = (char*)cJSON_malloc(len))) 
        return 0;
    memcpy(copy,str,len);
    return copy;
}

/*
*   Add the item to the head of cJSON array.
*/
void cJSON_AddItemToArrayR(cJSON *array, cJSON *item)
{
    cJSON *c=array->child;
    if (!item) 
        return; 
    if (!c) {
        array->child=item;
    } else {
        item->next=c;
        c->prev = item;
        array->child=item;
    }
}

/*
*   Add the item to the head of cJSON object.
*/
void cJSON_AddItemToObjectR(cJSON *object,const char *string,cJSON *item)    
{
    if (!item) 
        return; 
    if (item->string) 
        cJSON_free(item->string);
    item->string = cJSON_strdup(string);
    cJSON_AddItemToArrayR(object,item);
}

/*
*   Change the cJSON item to lists, include the key and value.
*   The size is (row+1)*colsize. The colsize mean the size of column. 
*/
int cJSON_ChangeToList(cJSON *obj, cJSON_list **list, int *size)
{
    cJSON *tmp;
    cJSON *pp, *cp, *p;
    int i, j;
    int arraySize;
	char *cstr;
	
    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
	//	获取object
	if (tmp->type == cJSON_Object) {
		tmp = tmp->child;
		if (!tmp)
			return -1;
	}
					
	{
		i = 0;
		pp = tmp;

		cp = pp;
		while (cp) {
			if (cp && cp->string) {
				i++;
			} else {
				
			}
			cp = cp->next;
		}
		*list = (cJSON_list*)malloc(sizeof(cJSON_list) * i);
		*size = i;
	}
	{
		pp = tmp;
		cp = pp;
		i = 0;
		while (cp) {
			if (cp && cp->string) {
				(*list)[i].titleName = cp->string;
				(*list)[i].listItem = cp;
				i++;
			}
			cp = cp->next;
		}
	}

    return 0;
}

/*
*   Set the value of which key you input.
*/
int cJSON_SetKeyValue(cJSON *obj, const char *key, const char *value)
{
    cJSON *tmp;
    cJSON *p;
    int i, j;
    int arraySize;
    string strtmp;
    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
    while (1) {
        if (!tmp) {
            if (p) {
                switch (p->type) {
                    case cJSON_Number:
                        break;
                    case cJSON_String:
                        break;
                }
            }
            break;
        }

        if (tmp->type == cJSON_Array) {
            arraySize = cJSON_GetArraySize(tmp);
            for (i = 0; i < arraySize; i++) {
                p = cJSON_GetArrayItem(tmp, i);
                    if (p) {
                        if (p->type == cJSON_Object)
                            cJSON_SetKeyValue(p, key, value);
                        else {
                            cJSON_SetKeyValue(p, key, value);
                        }
                    } else {

                    }
                }
            tmp = tmp->next;
        } else if (tmp->string && strcmp(tmp->string, "null")){
			if (strcmp(tmp->string, key) == 0) {
				switch (tmp->type) {
                case cJSON_Number:
                    tmp->valueint = atoi(value);
					tmp->valuedouble = atof(value);
                    break;
                case cJSON_String:
					tmp->valuestring = (char*)realloc((void*)tmp->valuestring, strlen(value) + 1);
                    memset(tmp->valuestring, 0, strlen(value) + 1);
                    memcpy(tmp->valuestring, value, strlen(value));
					break;
                default:
                    break;
				}
			}
            
            tmp = tmp->next;
        } else {
            switch (tmp->type) {
                case cJSON_Number:
                    
					getline(cin, strtmp);
                    tmp->valueint  = atoi(strtmp.c_str());
                    tmp->valuedouble = atof(strtmp.c_str());
                    break;
                case cJSON_String:
					
                    getline(cin, strtmp);
                    if (strtmp.size()) {
                        tmp->valuestring = (char*)realloc((void*)tmp->valuestring, strtmp.size() + 1);
                        memset(tmp->valuestring, 0, strtmp.size() + 1);
                        memcpy(tmp->valuestring, strtmp.c_str(), strtmp.size());
                    }
                    break;
            }
            tmp = tmp->next;
        }
    }

    return 0;
}

/*
*   Show all key and value, except the array in object which you input.
*/
int cJSON_Show(cJSON *obj)
{
    cJSON *tmp;
    cJSON *p;
    int i, j;
    int arraySize;

    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
    while (1) {
        if (!tmp) {
            if (p) {
                switch (p->type) {
                    case cJSON_Number:
                        printf("value:%d\n", p->valueint);
                        break;
                    case cJSON_String:
                        printf("value:%s\n", p->valuestring);
                        break;
                }
            }
            break;
        }

        if (tmp->type == cJSON_Array) {
            printf("array:%s, size:%d\n", tmp->string, cJSON_GetArraySize(tmp));
            arraySize = cJSON_GetArraySize(tmp);
            for (i = 0; i < arraySize; i++) {
                p = cJSON_GetArrayItem(tmp, i);
                    if (p) {
                        if (p->type == cJSON_Object)
                            cJSON_Show(p);
                        else {
                            cJSON_Show(p);
                        }
                    } else {
                        printf("what?\n");
                    }
                }
            tmp = tmp->next;
        } else if (tmp->string && strcmp(tmp->string, "null")){
            printf("obj:%s\t", tmp->string);
            switch (tmp->type) {
                case cJSON_Number:
                    printf("value:%d\n", tmp->valueint);
                    break;
                case cJSON_String:
                    printf("value:%s\n", tmp->valuestring);
                    break;
                default:
                    printf("value:\n");
                    break;
            }
            tmp = tmp->next;
        } else {
            switch (tmp->type) {
                case cJSON_Number:

                    printf("number:%lf", tmp->valuedouble);
                    break;
                case cJSON_String:
                    printf("string:%s\n", tmp->valuestring);
                    break;
            
            }
            tmp = tmp->next;
            //n++;
        }

    }

    return 0;
}

/*
*   Set all value of each item.
*/
int cJSON_SetAllItem(cJSON *obj)
{
    cJSON *tmp;
    cJSON *p;
    int i, j;
    int arraySize;
    string strtmp;
    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
    while (1) {
        if (!tmp) {
            if (p) {
                switch (p->type) {
                    case cJSON_Number:
                        
                        getline(cin, strtmp);
                        if (strtmp.size() <= 0) 
                        //  because nothing is input, then don't modify it
                            break;
                        p->valueint  = atoi(strtmp.c_str());
                        p->valuedouble = atof(strtmp.c_str());
                        break;
                    case cJSON_String:
                        
                        getline(cin, strtmp);
                        if (strtmp.size() <= 0)
                            break;
                        if (strtmp.size() > strlen(p->valuestring)) {
                        p->valuestring = (char*)realloc((void*)p->valuestring, 
                            strtmp.size() + 1);
                        memset(p->valuestring, 0, strtmp.size() + 1);
                        memcpy(p->valuestring, strtmp.c_str(), strtmp.size());
                        } else {
                            memset(p->valuestring, 0, strtmp.size() + 1);
                            memcpy(p->valuestring, strtmp.c_str(), strtmp.size());
                        }
                        break;
                }
            }
            break;
        }

        if (tmp->type == cJSON_Array) {
            arraySize = cJSON_GetArraySize(tmp);
            for (i = 0; i < arraySize; i++) {
                p = cJSON_GetArrayItem(tmp, i);
                    if (p) {
                        printf("The NO.%d items of %s array:\n", i, tmp->string);
                        if (p->type == cJSON_Object)
                            cJSON_SetAllItem(p);
                        else {
                            cJSON_SetAllItem(p);
                        }
                    } else {

                    }
                }
            tmp = tmp->next;
        } else if (tmp->string && strcmp(tmp->string, "null")) {
            cout << tmp->string << ": " ;
            switch (tmp->type) {
                case cJSON_Number:
                    
                    getline(cin, strtmp);
                    if (strtmp.size() <= 0)
                        break;
                    tmp->valueint  = atoi(strtmp.c_str());
                    tmp->valuedouble = atof(strtmp.c_str());
                    break;
                case cJSON_String:

                    getline(cin, strtmp);
                    if (strtmp.size() <= 0)
                        break;
                    if (strtmp.size() > strlen(tmp->valuestring)) {
                        tmp->valuestring = (char*)realloc((void*)tmp->valuestring, 
                            strtmp.size() + 1);
                        memset(tmp->valuestring, 0, strtmp.size() + 1);
                        memcpy(tmp->valuestring, strtmp.c_str(), strtmp.size());
                    } else {
                        memset(tmp->valuestring, 0, strtmp.size() + 1);
                        memcpy(tmp->valuestring, strtmp.c_str(), strtmp.size());
                    }
                    break;
                default:
                    break;
            }
            tmp = tmp->next;
        } else {
            switch (tmp->type) {
                case cJSON_Number:
                    
                    getline(cin, strtmp);
                    if (strtmp.size() <= 0)
                        break;
                    tmp->valueint  = atoi(strtmp.c_str());
                    tmp->valuedouble = atof(strtmp.c_str());
                    break;
                case cJSON_String:
                    
                    getline(cin, strtmp);
                    if (strtmp.size() <= 0)
                        break;
                    if (strtmp.size() > strlen(tmp->valuestring)) {
                        tmp->valuestring = (char*)realloc((void*)tmp->valuestring, 
                            strtmp.size() + 1);
                        memset(tmp->valuestring, 0, strtmp.size() + 1);
                        memcpy(tmp->valuestring, strtmp.c_str(), strtmp.size());
                    } else {
                        memset(tmp->valuestring, 0, strtmp.size() + 1);
                        memcpy(tmp->valuestring, strtmp.c_str(), strtmp.size());
                    }
                    break;
            }
            tmp = tmp->next;
        }
    }

    return 0;
}

/*
*   Set the string of key. The parameter of ok is old key string.
*/
int cJSON_SetItemKey(cJSON *obj, const char *ok, const char *nk)
{
    cJSON *tmp;
    cJSON *p;
    int i, j;
    int arraySize;

    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
    while (1) {
        if (!tmp) {
            if (p) {
                switch (p->type) {
                    case cJSON_Number:
                        
                        break;
                    case cJSON_String:
                        
                        break;
                }
            }
            break;
        }

        if (tmp->type == cJSON_Array) {
			if (!strcmp(tmp->string, ok)) {
				if (strlen(nk) > strlen(tmp->string)) {
				tmp->string = (char*)realloc((void*)tmp->string, strlen(nk) + 1);
				memset(tmp->string, 0, strlen(nk) + 1);
				memcpy(tmp->string, nk, strlen(nk));
				} else {
					memset(tmp->string, 0, strlen(nk) + 1);
					memcpy(tmp->string, nk, strlen(nk));
				}
			}
            
            arraySize = cJSON_GetArraySize(tmp);
            for (i = 0; i < arraySize; i++) {
                p = cJSON_GetArrayItem(tmp, i);
                    if (p) {
                        if (p->type == cJSON_Object)
                            cJSON_SetItemKey(p, ok, nk);
                        else {
                            cJSON_SetItemKey(p, ok, nk);
                        }
                    } else {
                        //printf("what?\n");
                    }
                }
            tmp = tmp->next;
        } else if (tmp->string && strcmp(tmp->string, "null")){
			if (!strcmp(tmp->string, ok)) {
				if (strlen(nk) > strlen(tmp->string)) {
				tmp->string = (char*)realloc((void*)tmp->string, strlen(nk) + 1);
				memset(tmp->string, 0, strlen(nk) + 1);
				memcpy(tmp->string, nk, strlen(nk));
				} else {
					memset(tmp->string, 0, strlen(nk) + 1);
					memcpy(tmp->string, nk, strlen(nk));
				}
			}            
            tmp = tmp->next;
        }
    }
    return 0;
}

/*
*   Show cJSON by tree, the parameter of n is retract.
*/
int cJSON_ShowByTree(cJSON *obj, int n)
{
    cJSON *tmp;
    cJSON *p;
	int i,j;
    int arraySize;
	int m;

    tmp = obj;
    if (!tmp)
        return -1;
    p = tmp;
    tmp = tmp->child;
    while (1) {
        if (!tmp) {
            if (p) {
				
                switch (p->type) {
                    case cJSON_Number:
						m = n - 3;if (m > 0) {while ((m--)) printf(" ");}
						m = n - 3 - 1;m&&printf("|");if (m > 0){while (m--) printf("_");}
                        printf("%-10d\n", p->valueint);
                        break;
                    case cJSON_String:
						m = n - 3;if (m > 0) {while ((m--)) printf(" ");}
						m = n - 3 - 1;m&&printf("|");if (m > 0){while (m--) printf("_");}
                        printf("%-10s\n", p->valuestring);
                        break;
                }
            }
            break;
        }

        if (tmp->type == cJSON_Array) {
			m = n - 3;if (m > 0) {while ((m--)) printf(" ");}
			m = n - 3 - 1;m&&printf("|");if (m > 0){while (m--) printf("_");}
            printf("array:%s, size:%d\n", tmp->string, cJSON_GetArraySize(tmp));
            arraySize = cJSON_GetArraySize(tmp);
            for (i = 0, m = n; i < arraySize; i++) {
                p = cJSON_GetArrayItem(tmp, i);
                    if (p) {
                        if (p->type == cJSON_Object)
                            cJSON_ShowByTree(p, m + 4);
                        else {
                            cJSON_ShowByTree(p, m + 4);
                        }
                    } else {
                        printf("what?\n");
                    }
                }
            tmp = tmp->next;
        } else if (tmp->string && strcmp(tmp->string, "null")){
			m = n - 3;if (m > 0) {while ((m--)) printf(" ");}
			m = n - 3 - 1;m&&printf("|");if (m > 0){while (m--) printf("_");}
            printf("%1s:", tmp->string);
            switch (tmp->type) {
                case cJSON_Number:
					
                    printf("%-10d\n", tmp->valueint);
                    break;
                case cJSON_String:
                    printf("%-10s\n", tmp->valuestring);
                    break;
                default:
                    printf("\n");
                    break;
            }
            tmp = tmp->next;
        } else {
            switch (tmp->type) {
                case cJSON_Number:
                    printf("%-10f\n", tmp->valuedouble);
                    break;
                case cJSON_String:
                    printf("%-10s\n", tmp->valuestring);
                    break;
            
            }
            tmp = tmp->next;
            //n++;
        }

    }

    return 0;
}
